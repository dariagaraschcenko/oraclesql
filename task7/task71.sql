WITH GRATH_SRC AS
 (SELECT 1 id, 100 From_src, 102 To_src, 100 Weight_src
    FROM dual
  UNION
  SELECT 2, 100, 101, 400
    FROM dual
  UNION
  SELECT 3, 101, 102, 200
    FROM dual
  UNION
  SELECT 4, 300, 301, 300
    FROM dual
  UNION
  SELECT 5, 300, 302, 400
    FROM dual
  UNION
  SELECT 6, 301, 302, 500
    FROM dual),
TEMP_GRATH AS
 (SELECT 100 From_temp, 101 To_temp, 200 weight
    FROM dual
  UNION
  SELECT 200, 201, 200
    FROM dual
  UNION
  SELECT 300, 301, 300
    FROM dual
  UNION
  SELECT 300, 302, 400
    FROM dual
  UNION
  SELECT 301, 302, 600
    FROM dual)
  SELECT G.From_src, T.From_temp, G.To_src, T.To_temp, G.Weight_src, T.weight FROM GRATH_SRC G  
  FULL JOIN TEMP_GRATH T ON G.From_src = T.From_temp AND G.To_src = T.To_temp
  WHERE G.Weight_src <> T.weight OR G.Weight_src IS NULL OR T.weight IS NULL
/*    
SELECT T.From_temp, T.To_temp, T.weight
  FROM TEMP_GRATH T
UNION
SELECT G.From_src, G.To_src, G.Weight_src
  FROM GRATH_SRC G
MINUS (SELECT From_temp, To_temp, weight
         FROM TEMP_GRATH
       INTERSECT
       SELECT G.From_src, G.To_src, G.Weight_src
         FROM GRATH_SRC G)
MINUS
SELECT T.From_temp, T.To_temp, G.Weight_src
  FROM TEMP_GRATH T
 INNER JOIN GRATH_SRC G
    ON G.From_src = T.From_temp AND G.To_src = T.To_temp*/
