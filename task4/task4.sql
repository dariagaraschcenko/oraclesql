select count(e.employee_id), max(e.salary)
  from employees e, departments d
 where e.department_id = d.department_id
   and d.department_name like 'Pu%'
 group by e.department_id;
